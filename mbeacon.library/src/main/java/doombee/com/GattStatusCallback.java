package doombee.com;

/**
 * Created by OEM2 on 8/8/2018.
 */

public interface GattStatusCallback {

    void onServiceStatusChanged(ServiceState status);
    void onGATTStatusChanged(int status);

}