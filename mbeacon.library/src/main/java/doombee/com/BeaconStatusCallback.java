package doombee.com;

import doombee.com.beacons.Beacon;
import doombee.com.classes.BLEDevice;

/**
 * Created by OEM2 on 8/8/2018.
 */

public interface BeaconStatusCallback {

    void onServiceStatusChanged(ServiceState status);
    void onBLEStatusChanged(int status);
    void onBeaconAdded(Beacon beacon);
    void onDeviceAdded(BLEDevice device);
    void onBeaconOutOfRange(Beacon beacon);
    void onDeviceOutOfRange(BLEDevice device);

    void onGATTStatusChanged(int status);

}