package doombee.com.beacons;

import android.util.Log;

import doombee.com.classes.UUIDInfo;

import java.util.ArrayList;

/**
 * Created by Claudio Ferraro on 08/12/2015.
 */
public class iBeacon extends Beacon {

    BeaconFrame thisFrame;

    public iBeacon(ArrayList<ADStructure> adStructures, byte[] data) {
        this.setADStructureList(adStructures);
        this.data = data;

        if (adStructures.size() > 1) {
            // store in the BeaconFrame only the relevant frame structure
            ADStructure frameMainStruct = adStructures.get(1);
            this.thisFrame = new BeaconFrame(frameMainStruct, data);
        }

    }

    @Override
    public Boolean updateData(byte[] data, ArrayList<ADStructure> adStructures) {

        this.setADStructureList(adStructures);
        this.data = data;

        if (adStructures.size() > 1) {
            // store in the BeaconFrame only the relevant frame structure
            ADStructure frameMainStruct = adStructures.get(1);

            thisFrame.setData(data);
            thisFrame.setFrameStructure(frameMainStruct);

            return true;
        }
        return false;
    }



    private Boolean isiBeacon() {

        byte[] scanRecord = this.data;

        if (scanRecord != null && scanRecord.length > 5) {
            int startByte = 2;
            boolean patternFound = false;
            while (startByte <= 5) {
                if (((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                        ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
                    patternFound = true;
                    break;
                }
                startByte++;
            }

            return patternFound;
        } else {
            return false;
        }


 //       return true;
    }

    @Override
    public Boolean isBeaconOfType() {

        if (this.isiBeacon()) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public Boolean isBeaconOfType(ArrayList<ADStructure> adStructures) {

        if (isiBeacon()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public iBeacon getBeaconInstance() {
        return this;
    }

    @Override
    public String getBeaconTypeName() {
        return "Ibeacon";
    }


    @Override
    public String getUuid() {

        byte[] currentData = thisFrame.getData();
        byte[] uuidBytes = new byte[16];

        System.arraycopy(currentData, 9, uuidBytes, 0, 16);
        String hexString = bytesToHex(uuidBytes);

        //Here is your UUID
        String uuid =  hexString.substring(0,8) + "-" +
                hexString.substring(8,12) + "-" +
                hexString.substring(12,16) + "-" +
                hexString.substring(16,20) + "-" +
                hexString.substring(20,32);
        return uuid;
    }

    @Override
    public Integer getMajor() {

        byte[] frame = thisFrame.getData();
        if (frame != null) {
            //Here is your Major value
            return (frame[21] & 0xff) * 0x100 + (frame[22] & 0xff);
        } else {
            return null;
        }
    }

    @Override
    public Integer getMinor() {

        byte[] frame = thisFrame.getData();
        if (frame != null) {
            //Here is your Minor value
            return (frame[23] & 0xff) * 0x100 + (frame[24] & 0xff);
        } else {
            return null;
        }
    }

    @Override
    public Integer getTxPower() {

        byte[] frame = thisFrame.getData();
        if (frame != null) {
            return (int) frame[25];
        } else {
            return null;
        }
    }
    /**
     * getUUIDAndMajorMinorFromScan
     */
    public UUIDInfo getUUIDInfo(int rssi) {

        byte[] scanRecord = this.data;

        String logString = "SCAN RECORD" + bytesToHex(scanRecord);

        int startByte = 2;
        boolean patternFound = false;
        while (startByte <= 5) {
            if (    ((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                    ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
                patternFound = true;
                break;
            }
            startByte++;
        }

        if (patternFound) {
            //Convert to hex String
            byte[] uuidBytes = new byte[16];
            System.arraycopy(scanRecord, startByte+4, uuidBytes, 0, 16);
            String hexString = bytesToHex(uuidBytes);

            //Here is your UUID
            String uuid =  hexString.substring(0,8) + "-" +
                    hexString.substring(8,12) + "-" +
                    hexString.substring(12,16) + "-" +
                    hexString.substring(16,20) + "-" +
                    hexString.substring(20,32);

            //Here is your Major value
            int major = (scanRecord[startByte+20] & 0xff) * 0x100 + (scanRecord[startByte+21] & 0xff);
            //Here is your Minor value
            int minor = (scanRecord[startByte+22] & 0xff) * 0x100 + (scanRecord[startByte+23] & 0xff);
            // System.out.println("UUID" + uuid + "Major " + major + " Minor " + minor);

            int txpower = (scanRecord[startByte+24]);

            System.out.println( logString + " TXPOW: " + txpower );
            double distance = calculateDistance(txpower, rssi);
            //  System.out.println("TXPOWER " + txpower + "  " + scanRecord[startByte+24] + " DISTANCE " + distance);
            UUIDInfo uuidinfo = new UUIDInfo(uuid, major, minor, txpower, distance);
            return uuidinfo;

        } else {


            return null;
        }

    }

    protected static double calculateDistance(int txPowerMeter, int rssi) {

        //txPowerMeter = -55;
        if (rssi == 0) {
            return -1.0D; // if we cannot determine distance, return -1.
        }

        double ratio = rssi * 1.0D / txPowerMeter;
        if (ratio < 1.0D) {
            return Math.pow(ratio, 10.0D);
        }
        else {
            double accuracy =  (0.89976D) * Math.pow(ratio , 7.7095D) + 0.111D;
            return accuracy;
        }
    }

    /**
     * bytesToHex method
     * Found on the internet
     * http://stackoverflow.com/a/9855338
     */
    static final char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
