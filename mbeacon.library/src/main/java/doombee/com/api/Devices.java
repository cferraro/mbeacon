package doombee.com.api;

import java.util.ArrayList;

import doombee.com.beacons.Beacon;
import doombee.com.classes.BLEDevice;
import doombee.com.mBeaconManager;

/**
 * Created by OEM2 on 8/26/2018.
 */

public class Devices {

    private mBeaconManager currentManager;

    public Devices(mBeaconManager manager) {
        this.currentManager = manager;
    }

    public ArrayList<BLEDevice> getDeviceList() {
        if (currentManager.mscanBluetoothService != null) {
            return currentManager.mscanBluetoothService.getDeviceList();
        } else {
            return null;
        }
    }

    public BLEDevice findDeviceByUUID(String uuid) {

        ArrayList<BLEDevice> list = this.getDeviceList();
        for (int i = 0; i < list.size(); i++) {
            BLEDevice bledevice = list.get(i);
            Beacon beacon = bledevice.getBeacon();
            if (beacon.getUuid() == uuid || beacon.getInstance() == uuid) {
                return bledevice;
            }
        }
        return null;
    }


}
