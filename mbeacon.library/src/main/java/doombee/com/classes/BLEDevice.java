package doombee.com.classes;

import android.bluetooth.BluetoothDevice;
import android.media.Image;

import doombee.com.beacons.Beacon;

import java.io.Serializable;
import java.util.Date;

public class BLEDevice implements Serializable {
	
	public transient BluetoothDevice bluDevice;
	public int weight;
	public Integer color;
	public Image logo;
	
	public UUIDInfo uuidinfo;
	
	public Date lastUpdated;

	public Beacon beacon;

	public String address;
	/*
	* Getters and setters
 	*/

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress() {
		return this.address;
	}

	public void setUUIDInfo(UUIDInfo uuidinfo) {
		this.uuidinfo = uuidinfo;
	}
	
	public UUIDInfo getUUIDInfo() {
		return this.uuidinfo;
	}
	
	public void setDevice(BluetoothDevice bluDevice) {
		this.bluDevice = bluDevice;
	}
	
	public BluetoothDevice getDevice() {
		return this.bluDevice;
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public int getWeight() {
		return this.weight;
	}
	
	public void setColor(int color) {
		this.color = color;
	}
	
	public Integer getColor() {
		return this.color;
	}
	
	public void setLogo(Image logo) {
		this.logo = logo;
	}
	
	public Image getLogo() {
		return logo;
	}
	
	public void setLastUpdated(Date lastupdated) {
		this.lastUpdated = lastupdated;
	}
	
	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setBeacon(Beacon beacon) {
		this.beacon = beacon;
	}

	public Beacon getBeacon() {
		return this.beacon;
	}
}
