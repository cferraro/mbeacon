package doombee.sunbyte.com.mbeacon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import doombee.com.BeaconStatusCallback;
import doombee.com.ServiceState;
import doombee.com.beacons.Beacon;
import doombee.com.classes.BLEDevice;
import doombee.com.mBeaconManager;

public class MbeaconActivity extends AppCompatActivity {

    mBeaconManager mManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mBeaconManager mManager = new mBeaconManager(this, beaconCallback, null);
        setContentView(R.layout.activity_mbeacon);

    }


    BeaconStatusCallback beaconCallback = new BeaconStatusCallback() {
        @Override
        public void onServiceStatusChanged(ServiceState status) {

        }

        @Override
        public void onBLEStatusChanged(int status) {

        }

        @Override
        public void onBeaconAdded(Beacon beacon) {

            Log.d("mbeacon", "ADDED NEW BEACON");

        }

        @Override
        public void onDeviceAdded(BLEDevice device) {

        }

        @Override
        public void onBeaconOutOfRange(Beacon beacon) {

        }

        @Override
        public void onDeviceOutOfRange(BLEDevice device) {

        }

        @Override
        public void onGATTStatusChanged(int status) {

        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mManager.stopBeaconManager();

    }
}
