package doombee.com.beacons;

import java.util.ArrayList;

/**
 * Created by Claudio Ferraro on 2/21/2018.
 *
 * Defines a Device which transmits Eddystone and iBeacon Frame at the same time
 *
 * Merges to existing Eddystone Beacon Class with IBeacon methods
 *
 */

public class HybridEddystoneIBeacon extends Beacon {

    iBeacon ibeacon;
    Eddystone eddystone;

    public HybridEddystoneIBeacon(iBeacon beaconi, Eddystone beacone) {
        ibeacon = beaconi;
        eddystone = beacone;
    }

    // ibeacon methods
    @Override
    public String getUuid() {
        return ibeacon.getUuid();
    }

    @Override
    public Integer getMajor() {
        return ibeacon.getMajor();
    }

    @Override
    public Integer getMinor() {
        return ibeacon.getMinor();
    }

    @Override
    public Integer getTxPower() {
        return ibeacon.getTxPower();
    }

    @Override
    public String getBeaconTypeName() {
        return "HybridEddystoneIBeacon";
    }

    public Boolean updateData(byte[] data, ArrayList<ADStructure> adStructures, String beaconType) {

        if (beaconType == "iBeacon") {
            ibeacon.updateData(data, adStructures);
        }

        if (beaconType == "Eddystone") {
            eddystone.updateData(data, adStructures);
        }
        return true;
    }
}
