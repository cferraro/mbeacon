package doombee.sunbyte.com.mbeacon;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * Created by OEM2 on 1/4/2018.
 */

public class mBeaconManager {


    private BeaconScanLeService mscanBluetoothService;

    private static final int STATE_CONNECTIONERROR = -1;

    private int currentStatus;


    public interface BeaconStatusCallback {

        void onBeaconStatusChanged(String result);
        void onBLEconnectionStatusChanged(int status);
    }

    public Boolean startBeaconManager(final Context ctx, final BeaconStatusCallback callback) {


        // is Bluetooth enabled ?
        System.out.println("BIND service");

        if (callback != null) {

            // RUN SCRIPT THAT WILL BE CONTINUOUSLY EXECUTED
            final Handler handler = new Handler();
            final int delay = 1000; //milliseconds

            handler.postDelayed(new Runnable() {
                public void run() {
                    callback.onBLEconnectionStatusChanged(currentStatus);
                    handler.postDelayed(this, delay);
                }
            }, delay);
        }

        Intent scanServiceIntent = new Intent(ctx, BeaconScanLeService.class);
        ctx.bindService(scanServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        return false;
    }







}
