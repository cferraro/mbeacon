package doombee.com;

/**
 * Created by OEM2 on 8/8/2018.
 */

public enum ServiceState {

    STATE_SERVICE_CONNECTED, STATE_CONNECTIONERROR, STATE_SERVICE_DISCONNECTED
}
