package doombee.com.beacons;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Claudio Ferraro on 08/12/2015.
 */
public class Beacon implements Serializable {

    ArrayList<? extends Beacon> beaconTypesList = new ArrayList<Beacon>();

    byte[] data;

    ArrayList<ADStructure> lOfADStructures;

    Object beaconInstance;
    String beaconType;

    String uuid;
    Integer minor;
    Integer major;
    Integer companyid;
    Integer beacontype;

    String namespace;
    String instance;
    Short rfu;
    Integer TxPower;
    String ephemeralID;
    Integer urlPrefix;
    String encodedUrl;
    Short batteryValue;
    Short temperatureValue;
    Integer PDUcount;
    Integer time;
    Integer version;



    public void setBeaconInstance(Object beaconInst) {
        this.beaconInstance = beaconInst;
    }
    public Object getBeaconInstance() {
        return this.beaconInstance;
    }

    public void setBeaconTypeName(String typeName) {
        this.beaconType = typeName;
    }
    public String getBeaconTypeName() {
        return "Beacon Base";
    }



    public Boolean isBeaconOfType() {
        return null;
    }

    public Boolean isBeaconOfType(ArrayList<ADStructure> adStructures) { return null; }

    // constructor
    Beacon() {
        this.lOfADStructures = new ArrayList<ADStructure>();
    }


    public void setADStructureList(ArrayList<ADStructure> list) {
        this.lOfADStructures = list;
    }

    public ArrayList<ADStructure> getADStructureList() {
        return this.lOfADStructures;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public byte[] getData() { return this.data; }

    public Boolean updateData(byte[] data, ArrayList<ADStructure> adStructures) { return null; }

    public Boolean updateData(byte[] data, ArrayList<ADStructure> adStructures, String beaconType) { return null; }

    // beacon specifics

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getMinor() { return this.minor; }

    public void setMinor(Integer minor) { this.minor = minor; }

    public Integer getMajor() { return this.major; }

    public void setMajor(Integer major) { this.major = major; }

    public Integer getCompanyId() { return this.companyid; }

    public void setCompanyId(Integer companyid) { this.companyid = companyid; }

    public Integer getBeaconType() { return this.beacontype; }

    public void setBeaconType(Integer beacontype) { this.beacontype = beacontype; }

    public String getNamespace() {
        return this.namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getInstance() {
        return this.instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public void setRFU(Short rfu) {
        this.rfu = rfu;
    }

    public Short getRFU() {
        return this.rfu;
    }

    public void setTxPower(Integer txPower) {
        this.TxPower = txPower;
    }

    public Integer getTxPower() {
        return this.TxPower;
    }

    public void setEphemeralID(String ephemeralID) {
        this.ephemeralID = ephemeralID;
    }

    public String getEphemeralID() {
        return this.ephemeralID;
    }

    public void setUrlPrefix(int urlPrefix) {
        this.urlPrefix = urlPrefix;
    }

    public int getUrlPrefix() {
        return this.urlPrefix;
    }

    public void setEncodedUrl(String encodedUrl) {
        this.encodedUrl = encodedUrl;
    }

    public String getEncodedUrl() {
        return this.encodedUrl;
    }

    public void setPDUcount(Integer PDUcount) {
        this.PDUcount = PDUcount;
    }

    public Integer getPDUcount() {
        return this.PDUcount;
    }

    public Short getBatteryValue() {
        return this.batteryValue;
    }

    public void setBatteryValue(Short batteryValue) {
        this.batteryValue = batteryValue;
    }

    public Short getTemperatureValue() {
        return this.temperatureValue;
    }

    public void setTemperatureValue(Short temperatureValue) {
        this.temperatureValue = temperatureValue;
    }

    public Integer getVersion() {
        return this.version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getTime() {
        return this.time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }
}
