package doombee.com;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import doombee.com.beacons.Beacon;
import doombee.com.classes.BLEDevice;

/**
 * Created by OEM2 on 8/7/2018.
 */

public class ServiceReceivers {

    BeaconStatusCallback scanCallback;
    GattStatusCallback gattCallback;


    public void registerScanReceiver(Context ctx, BeaconStatusCallback callback) {

        ctx.registerReceiver(mBleScanUpdateReceiver, makeScanUpdateIntentFilter());
        this.scanCallback = callback;

    }

    public void registerGattReceiver(Context ctx, GattStatusCallback gattCallback) {

        ctx.registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        this.gattCallback = gattCallback;
    }

    // SCAN RECEIVER

    private static IntentFilter makeScanUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ScanState.ACTION_NEW_BEACON_DETECTED);
        intentFilter.addAction(ScanState.ACTION_BEACON_OUT_OF_RANGE);
        return intentFilter;
    }


    // Code to manage Service Broadcasts
    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    public final BroadcastReceiver mBleScanUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final String action = intent.getAction();
            if (ScanState.ACTION_NEW_BEACON_DETECTED.equals(action)) {
                Beacon beacon = (Beacon)intent.getSerializableExtra(ScanState.ACTION_NEW_BEACON_DETECTED);
                if (scanCallback != null) {
                    scanCallback.onBeaconAdded(beacon);
                }
            } else if (ScanState.ACTION_NEW_DEVICE_DETECTED.equals(action)) {
                BLEDevice device = (BLEDevice)intent.getSerializableExtra(ScanState.ACTION_NEW_DEVICE_DETECTED);
                if (scanCallback != null) {
                    scanCallback.onDeviceAdded(device);
                }
            }


        }
    };


    // GATT RECEIVER

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ScanState.ACTION_NEW_BEACON_DETECTED);
        intentFilter.addAction(ScanState.ACTION_BEACON_OUT_OF_RANGE);
        return intentFilter;
    }


    // Code to manage Service Broadcasts
    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    public static final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {

            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {

            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {

            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {

            }
        }
    };
}
