package doombee.com;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by OEM2 on 8/7/2018.
 */

public class ServiceConnections {

    BeaconScanLeService mscanService;
    BluetoothLeService mbleService;

    BeaconStatusCallback beaconManagerCallback;
    GattStatusCallback gattManagerCallback;

    public ServiceConnections(BeaconScanLeService scanService, BluetoothLeService bleService,
                              BeaconStatusCallback beaconManagerCallback, GattStatusCallback gattManagerCallack) {

        this.mscanService = scanService;
        this.mbleService = bleService;

        this.beaconManagerCallback = beaconManagerCallback;
        this.gattManagerCallback = gattManagerCallback;
    }


    // Code to manage Service lifecycle.
    public final ServiceConnection mScanServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {

            if (beaconManagerCallback != null) {
                beaconManagerCallback.onServiceStatusChanged(ServiceState.STATE_SERVICE_CONNECTED);
            }

            mscanService = ((BeaconScanLeService.LocalBinder) service).getService();

            if (!mscanService.initialize()) {
                if (beaconManagerCallback != null) {
                    beaconManagerCallback.onServiceStatusChanged(ServiceState.STATE_CONNECTIONERROR);
                }
            }
            // Automatically connects to the device upon successful start-up initialization.
            // mscanBluetoothService.connect(mDeviceAddress);
        }
        @Override
        public void onServiceDisconnected(ComponentName componentName) {

            if (beaconManagerCallback != null) {
                beaconManagerCallback.onServiceStatusChanged(ServiceState.STATE_SERVICE_DISCONNECTED);
            }
            mscanService = null;
        }
    };

    // Code to manage Service lifecycle.
    public final ServiceConnection mGattServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {

            if (gattManagerCallback != null) {
                gattManagerCallback.onServiceStatusChanged(ServiceState.STATE_SERVICE_CONNECTED);
            }

            mbleService = ((BluetoothLeService.LocalBinder) service).getService();

            if (!mbleService.initialize()) {
                if (gattManagerCallback != null) {
                    gattManagerCallback.onServiceStatusChanged(ServiceState.STATE_CONNECTIONERROR);
                }
            }
            // Automatically connects to the device upon successful start-up initialization.
            // mscanBluetoothService.connect(mDeviceAddress);
        }
        @Override
        public void onServiceDisconnected(ComponentName componentName) {

            if (gattManagerCallback != null) {
                gattManagerCallback.onServiceStatusChanged(ServiceState.STATE_SERVICE_DISCONNECTED);
            }
            mbleService = null;
        }
    };
}
