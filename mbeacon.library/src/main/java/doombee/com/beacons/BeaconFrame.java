package doombee.com.beacons;

import java.io.Serializable;

/**
 * Created by Claudio Ferraro on 08/12/2015.
 */
public class BeaconFrame implements Serializable {

    byte[] data;
    int length;
    byte type;
    ADStructure frameStructure;

    // constructor
    public BeaconFrame(ADStructure adStructure, byte[] data) {
        this.frameStructure = adStructure;
        this.setData(data);
    }

    public void setType(byte type) {
        this.type = type;
    }

    public byte getType() {
        return this.type;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public byte[] getData() { return this.data; }

    public void setFrameStructure(ADStructure frameStructure) {
        this.frameStructure = frameStructure;
    }

    public ADStructure getFrameStructure() {
        return this.frameStructure;
    }

}
