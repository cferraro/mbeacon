package doombee.com.beacons;

import java.io.Serializable;

/**
 * Created by Claudio Ferraro on 08/12/2015.
 */
public class ADStructure implements Serializable {

    byte[] data;
    int length;
    int type;

    // constructor
    public ADStructure(byte[] data, int length, int type) {
        this.setData(data);
        this.length = length;
        this.type = type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return this.type;
    }

    public void setParentType(int parentType) {

    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public byte[] getData() { return this.data; }


}
