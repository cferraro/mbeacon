package doombee.com;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;

import doombee.com.classes.BLEDevice;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * Created by OEM2 on 1/4/2018.
 */

public class mBeaconManager {

    ServiceConnections servicesConns;

    public BeaconScanLeService mscanBluetoothService;
    public BluetoothLeService mBleService;
    private Context currentContext;

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    private static final int STATE_CONNECTIONERROR = -1;
    private static final int STATE_SERVICE_CONNECTED = 1;
    private static final int STATE_SERVICE_DISCONNECTED = 0;
    private static final int STATE_BLE_CONNECTED = 2;
    private static final int STATE_BLE_DISCONNECTED = 3;
    private static final int STATE_GATT_CONNECTED = 4;
    private static final int STATE_GATT_DISCONNECTED = 5;

    private int currentStatus;

    private ArrayList<BLEDevice> deviceList;


    BeaconStatusCallback beaconManagerCallback;
    GattStatusCallback gattManagerCallback;

    public mBeaconManager(final Activity ctx, final BeaconStatusCallback callback,
                          final GattStatusCallback gattcallback) {
        this.startBeaconManager(ctx, callback, gattcallback);
    }

    protected Boolean startBeaconManager(final Activity ctx, final BeaconStatusCallback callback,
                                      final GattStatusCallback gattcallback) {

        ServiceReceivers receivers = new ServiceReceivers();

        beaconManagerCallback = callback;
        gattManagerCallback = gattcallback;

        servicesConns = new ServiceConnections(this.mscanBluetoothService, this.mBleService,
                                               this.beaconManagerCallback, this.gattManagerCallback);
        this.currentContext = ctx;

        this.deviceList = new ArrayList<BLEDevice>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check
            if (ctx.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ctx.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
            }

        }


        // CREATE BLE SCAN SERVICE
        // is Bluetooth enabled ?
        Log.d("mbeacon", "BIND SERVICE");
        Intent scanServiceIntent = new Intent(ctx, BeaconScanLeService.class);
        ctx.bindService(scanServiceIntent, servicesConns.mScanServiceConnection, BIND_AUTO_CREATE);

        // REGISTER BLE SCAN RECEIVER
        receivers.registerScanReceiver(ctx, beaconManagerCallback);

        // CREATE GATT SERVICE
        Intent gattServiceIntent = new Intent(ctx, BluetoothLeService.class);
        ctx.bindService(gattServiceIntent, servicesConns.mGattServiceConnection, BIND_AUTO_CREATE);

        receivers.registerGattReceiver(ctx, gattManagerCallback);

        Log.d("mbeacon", "BIND SERVICE2");
        return false;
    }

    public Boolean stopBeaconManager() {
        if (mscanBluetoothService != null) {
            currentContext.unbindService(servicesConns.mScanServiceConnection);
            mscanBluetoothService = null;
            return true;
        }
        return false;
    }

    private boolean isServiceRunning(Class<?> serviceClass, Context ctx) {
        ActivityManager manager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {

            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }





}
