package doombee.com;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import doombee.com.beacons.Beacon;
import doombee.com.beacons.BeaconParser;
import doombee.com.classes.BLEDevice;
import doombee.com.util.Ascii;

public class BeaconScanLeService extends Service {

    private ArrayList<BLEDevice> deviceList;

    private final static String TAG = BeaconScanLeService.class.getSimpleName();
    private BluetoothManager mBluetoothManager;

    private String mBluetoothDeviceAddress;
    private int mConnectionState = STATE_DISCONNECTED;
    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;

    // 20 seconds by default
    private int mInterval = 20000;
    // Stops scanning after 5 seconds.
    private static final long SCAN_PERIOD = 5000;

    private Handler mHandler;
    private Handler mHandlerMain;

    private void scanLeDevice(final boolean enable) {

        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                }
            }, SCAN_PERIOD);

            mScanning = true;
           Boolean result = mBluetoothAdapter.startLeScan(mLeScanCallback);

        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }

    }

    Runnable mScannerRunTask = new Runnable() {
        @Override
        public void run() {
            Log.d("mbeacon", "scannerrun");
            scanLeDevice(true);
            mHandlerMain.postDelayed(mScannerRunTask, mInterval);
        }
    };


    void startRepeatingTask() {
        mScannerRunTask.run();
    }
    void stopRepeatingTask() {
        mHandlerMain.removeCallbacks(mScannerRunTask);
    }

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {

                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {

                            boolean exists = false;
                            BeaconParser beaconParser = new BeaconParser();
                            for (int i = 0; i < deviceList.size(); i++) {

                                BluetoothDevice compareDevice = deviceList.get(i).getDevice();

                                if (device.getAddress().equals(compareDevice.getAddress().toString())) {
                                    deviceList.get(i).setLastUpdated(new Date());

                                   // if (compareDevice.getAddress().compareTo( "AC:23:3F:24:98:81") == 0) {
                                   //     Log.d("mbeacon", compareDevice.getAddress().toString());
                                        // NEW BEACON LIBRARY
                                    Beacon actualBeacon = deviceList.get(i).getBeacon();
                                    deviceList.get(i).setBeacon(beaconParser.parse(scanRecord, actualBeacon));
                                      //  Log.d("mbeacon", "BEACON NAME:" + actualBeacon.getBeaconTypeName());
                                        // END new library

                                   // }
                                    exists = true;
                                    break;
                                }
                            }
                            if (!exists) {
                                Log.d("mbeacon","NEW DEVICE " + device.getAddress().toString());

                                //if (device.getAddress().compareTo( "AC:23:3F:24:98:81") == 0) {
                                    // UUIDInfo uuidinfo = getUUIDInfoFromScan(scanRecord, rssi);

                                    // NEW BEACON LIBRARY
                                    Beacon relatedBeacon = beaconParser.parse(scanRecord, null);
                                    if (relatedBeacon != null) {

                                        BLEDevice newDevice = new BLEDevice();
                                        newDevice.setDevice(device);
                                        newDevice.setLastUpdated(new Date());

                                        Log.d("mbeacon", "BEACON UUID " + relatedBeacon.getUuid() + " FOR ADDRESS " + device.getAddress());
                                        newDevice.setBeacon(relatedBeacon);
                                        // END new library
                                        //newDevice.setUUIDInfo(uuidinfo);
                                        deviceList.add(newDevice);
                                        broadcastUpdate(ScanState.ACTION_NEW_DEVICE_DETECTED, newDevice, null);
                                        broadcastUpdate(ScanState.ACTION_NEW_BEACON_DETECTED, null, relatedBeacon);
                                        Log.d("mbeacon", "ADDED DEVICE " + relatedBeacon.getBeaconTypeName() + " ADDRESS: " + device.getAddress());
                                    }

                            } else {

                                // finally refresh interface
                                // todo this invalidate is called too
                                //  bView.invalidate();
                                // bView.RefreshBrowserDesignForNewDevice(blofDevices.size(), blofDevices);
                            }

                            // clean up
                            // remove out of range devices: not updated since
                            for (int i = deviceList.size() - 1; i >= 0; i--) {
                                Date checkdate = deviceList.get(i).getLastUpdated();
                                Date currentDate = new Date();

                                long seconds = (currentDate.getTime()-checkdate.getTime())/1000;
                                long timeoutLimit = (SCAN_PERIOD + (mInterval * 2)) / 1000;

                                if (seconds > timeoutLimit) {
                                    // remove device, it's out of range
                                    Log.d("mbeacon", "Removing Device " + i);
                                    deviceList.remove(i);
                                    //bView.invalidate();
                                    //bView.RefreshBrowserDesignForNewDevice(blofDevices.size(), blofDevices);

                                }
                            }





                            /*    Log.d("mbeacon", "BLUETOOTH SCAN");
                            for (int i = 0; i < deviceList.size(); i++) {

                                BluetoothDevice compareDevice = deviceList.get(i).getDevice();
                                if (device.getAddress().compareTo(compareDevice.getAddress()) == 0) {
                                    deviceList.get(i).setLastUpdated(new Date());

                                    double newdistance = getDistanceFromScan(scanRecord, rssi);
                                    deviceList.get(i).getUUIDInfo().setDistance(newdistance);

                                    exists = true;
                                    break;
                                }
                            }
                            if (!exists) {

                                BLEDevice newDevice = new BLEDevice();
                                newDevice.setDevice(device);
                                newDevice.setLastUpdated(new Date());

                                UUIDInfo uuidinfo = getUUIDInfoFromScan(scanRecord, rssi);

                                // NEW BEACON LIBRARY
                                BeaconParser beaconParser = new BeaconParser();
                                beaconParser.parse(scanRecord);
                                // END new library

                                newDevice.setUUIDInfo(uuidinfo);

                                System.out.println("ADDED DEVICE" + device.getAddress() + " rssi " +
                                        Integer.toString(rssi) + " scanRecord " + scanRecord);
                                blofDevices.add(newDevice);

                                // finally refresh interface
                                //bView.invalidate();
                                //bView.RefreshBrowserDesignForNewDevice(blofDevices.size(), blofDevices);

                            } else {

                                // finally refresh interface
                                // todo this invalidate is called too
                                //bView.invalidate();
                               // bView.RefreshBrowserDesignForNewDevice(blofDevices.size(), blofDevices);
                            }

                            // clean up
                            // remove out of range devices: not updated since
                            for (int i = blofDevices.size() - 1; i >= 0; i--) {
                                Date checkdate = blofDevices.get(i).getLastUpdated();
                                Date currentDate = new Date();

                                long seconds = (currentDate.getTime()-checkdate.getTime())/1000;

                                long timeoutLimit = (SCAN_PERIOD + (mInterval / 2)) / 1000;
                                // System.out.println("cur" + currentDate.getTime() + " CHECK" + checkdate.getTime() +
                                // " SECONDS " + seconds + " timout " + timeoutLimit);
                                if (seconds > timeoutLimit) {
                                    // remove device, it's out of range
                                    blofDevices.remove(i);
                                    //bView.invalidate();
                                    //bView.RefreshBrowserDesignForNewDevice(blofDevices.size(), blofDevices);

                                }
                            }*/

                        }
                    });

                }
            };



    public ArrayList<BLEDevice> getDeviceList() {
        return this.deviceList;
    }




    //broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
    private void broadcastUpdate(String action, BLEDevice device, Beacon beacon) {
        final Intent intent = new Intent(action);

        if (device != null) {
            intent.putExtra(ScanState.ACTION_NEW_DEVICE_DETECTED, device);
        }
        if (device != null) {
            intent.putExtra(ScanState.ACTION_NEW_BEACON_DETECTED, beacon);
        }
        sendBroadcast(intent);
    }

    public class LocalBinder extends Binder {
        public BeaconScanLeService getService() {
            return BeaconScanLeService.this;
       
        }
    }
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
    private final IBinder mBinder = new LocalBinder();


    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {

        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        mHandler = new Handler();
        mHandlerMain = new Handler();

        deviceList = new ArrayList<BLEDevice>();

        Log.d("mbeacon", "Initializing service");

        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.d("mbeacon", "Unable to initialize BluetoothManager.");
                return false;
            }
        }
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Log.d("mbeacon", "Unable to obtain a BluetoothAdapter. Bluetooth may be disabled");
            return false;
        }

        // start ble scanner
        this.startRepeatingTask();

        return true;
    }


}