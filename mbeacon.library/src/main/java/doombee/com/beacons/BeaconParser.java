package doombee.com.beacons;

import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Claudio Ferraro on 08/12/2015.
 */
public class BeaconParser {

    ArrayList<String> typeNames = new ArrayList<String>();
    ArrayList<ADStructure> structList;
    byte[] data;
    Beacon resultBeacon;

    // constructor
    public BeaconParser() {
        ArrayList<ADStructure> list = new ArrayList<ADStructure>();
        this.loadBeaconModels();
    }

    public void loadBeaconModels() {

        // beacon manifest. Add new beacon types here
        //List<Object> beaconTypesList = new ArrayList<Object>();
        typeNames.add("iBeacon");
        typeNames.add("Eddystone");

    }

    public Beacon parse(byte[] scanRecord, Beacon beacon) {


        this.data = scanRecord;

        if (scanRecord == null) return null;
        this.loadBeaconModels();

        // If parameters are wrong.
        //if (offset < 0 || length < 0 || payload.length <= offset)
        //{
            // Simply return an empty list without throwing an exception.
        //    return list;
        //}

        // The index to terminate the valid range.
        //int end = Math.min(length, payload.length);
        ArrayList<ADStructure> list = new ArrayList<ADStructure>();

        for (int i = 0; i < scanRecord.length; )
        {
            // The first octet represents 'length'.
            int framelen = scanRecord[i] & 0xFF;

            System.out.println("L" + framelen + " ");
            // If the length is zero.
            if (framelen == 0) break;

            if ((scanRecord.length - i - 1) < framelen)
            {
                // Remaining bytes are insufficient.
                break;
            }

            // The second octet represents 'type'.
            int frametype = scanRecord[i + 1] & 0xFF;

            // The AD data.
            byte[] framedata = Arrays.copyOfRange(scanRecord, i + 1, i + framelen + 1);

            // Create a beacon frame
            ADStructure bf = new ADStructure(framedata, framelen, frametype);

            list.add(bf);

            //if ( MACAddress.compareTo( "AB:CD:EF:03:00:04") == 0) {
            //    System.out.println("Added Structure" + "L" + framedata.length + "D" + new String(framedata) + "\n\r");
           // }

            // next frame
            i += 1 + framelen;
        }

        this.structList = list;
        return this.instantiateBeacon(beacon);
    }


    private Boolean isBeaconOfType(String type) {

        try {

            Class myclass = Class.forName("doombee.com.beacons." + type);

            //Use reflection to list methods and invoke them
            Method method = myclass.getDeclaredMethod("isBeaconOfType");
            Object obj = myclass.getDeclaredConstructor(ArrayList.class, byte[].class).newInstance(this.structList, this.data);

            Boolean value = (Boolean) method.invoke(obj);
            if (value) {
                this.resultBeacon = (Beacon) obj;
            }
            return value;

        } catch (InvocationTargetException e) {
            StackTraceElement[] trace = e.getStackTrace();
            System.out.println(e);

        } catch (NoSuchMethodException e) {
            System.out.println(e);

        } catch (IllegalAccessException e) {
            System.out.println(e);

        } catch (InstantiationException e) {
            System.out.println(e);
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        }

        return false;
    }

    private String getBeaconType() {

        for (int i = 0; i < typeNames.size(); i++) {
            if (isBeaconOfType(typeNames.get(i))) {
                return this.resultBeacon.getBeaconTypeName();
            }
        }
        return null;
    }

    private Beacon instantiateBeacon(Beacon beacon) {

        if (beacon == null) {
            for (int i = 0; i < typeNames.size(); i++) {

                if (isBeaconOfType(typeNames.get(i))) {
                    return this.resultBeacon;
                }

            }

        } else {

            String beaconType = beacon.getBeaconTypeName();

            if (beacon.isBeaconOfType(this.structList) != null) {
                if (!beacon.isBeaconOfType(this.structList)) {
                    // different beacon type for the same MAC Address detected
                    // Creating hybrid Device
                    iBeacon iBeacon = null;
                    Eddystone eddystone = null;

                    if (beacon.getBeaconTypeName() == "iBeacon") {
                        iBeacon = (iBeacon) beacon;
                    }

                    if (beacon.getBeaconTypeName() == "Eddystone") {
                        eddystone = (Eddystone) beacon;
                    }

                    String currentType = this.getBeaconType();
                    if (currentType != null) {

                        if (currentType == "iBeacon") {
                            iBeacon = (iBeacon) this.resultBeacon;
                        }

                        if (currentType == "Eddystone") {
                            eddystone = (Eddystone) this.resultBeacon;
                        }

                        HybridEddystoneIBeacon hybridBeacon = new HybridEddystoneIBeacon(iBeacon, eddystone);
                        return hybridBeacon;

                    } else {
                        return beacon;
                    }

                } else {

                    Boolean result;
                    String currentType = this.getBeaconType();

                    if (beacon.getBeaconTypeName() != "HybridEddystoneIBeacon") {
                        // beacon already exists. Just update the existing data
                        result = beacon.updateData(data, this.structList);
                    } else {
                        result = beacon.updateData(data, this.structList, currentType);
                    }
                }

                //System.out.println("Updating data result " + result);

                //System.out.println("ENCODED URL" + beacon.getEncodedUrl() + "\n\r");
                //System.out.println("Instance is" + beacon.getInstance() + " Namespace" + beacon.getNamespace()  + "\n\r");
                //System.out.println("Prefix is" + beacon.getUrlPrefix() + "\n\r");
                System.out.println(" Battery" + beacon.getBatteryValue()  + "\n\r");
                System.out.println("Temperature" + beacon.getTemperatureValue() + " PDU Count" + beacon.getPDUcount() + "\n\r");
                System.out.println( " Time" + beacon.getTime()  + "\n\r");
                System.out.println("Version" + beacon.getVersion()  + "\n\r");
                System.out.println(" RFU " + beacon.getRFU()  + "\n\r");
                System.out.println(" TXPOWER " + beacon.getTxPower() + "\n\r" );
                System.out.println(" EPHEMERAL " + beacon.getEphemeralID() + "\n\r");
            }




            return beacon;

        }

        return null;
    }

}
