package doombee.com.beacons;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Claudio Ferraro on 08/12/2015.
 */
public class Eddystone extends Beacon {

    ArrayList<BeaconFrame> Frames = new ArrayList<BeaconFrame>();
    ArrayList<Byte> FrameTypes = new ArrayList<Byte>();
    Byte currentFrameType;

    // constructor
    public Eddystone(ArrayList<ADStructure> adStructures, byte[] data) {

        this.setADStructureList(adStructures);
        this.data = data;

        if (adStructures.size() > 2) {
            // store in the BeaconFrame only the relevant frame structure
            ADStructure frameMainStruct = adStructures.get(2);
            this.addNewFrame(frameMainStruct, data);
        }

    }

    private void addNewFrame(ADStructure adStructure, byte[] data) {
        // creates a BeaconFrame and adds to it the current AdStructure List
        BeaconFrame newFrame = new BeaconFrame(adStructure, data);

        newFrame.setType(this.getFrameType());
        Frames.add(newFrame);
        // Add at the same index a frame type
        FrameTypes.add(newFrame.getType());

        // set the TXPOWER
        byte[] frameData = adStructure.getData();
        if (frameData.length > 4) {
            this.setTxPower((int)frameData[4]);
        }

        //System.out.println("INDEX " + (Frames.size() - 1) + " ADDED NEW FRAME OF TYPE " + String.valueOf(newFrame.getType()) + " " + new String(newFrame.getData()) + "\n\r");
    }

    private Byte getFrameType() {
        // parse the incoming frame: check the frame type
        byte frametype;
        ArrayList<ADStructure> adStruct = this.getADStructureList();
        // frame type is the fifth byte of the third ad structure
        if (adStruct.size() > 2) {
            byte[] data = adStruct.get(2).getData();
            if (data.length > 4) {
                frametype = data[3];
            } else {
                return null; // data corrupted
            }
        } else {
            return null; // data corrupted
        }
        return frametype;
    }


    private Boolean FrameExists() {

        byte frametype = this.getFrameType();

        this.currentFrameType = frametype;
        // current transmitted frame is of type frametype
        if (this.FrameTypes.contains(frametype)) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public String getBeaconTypeName() {
        return "Eddystone";
    }

    // updates the data or adds a new frame if not exists
    // returns false if no data added
    @Override
    public Boolean updateData(byte[] data, ArrayList<ADStructure> adStructures) {

        this.setADStructureList(adStructures);
        this.data = data;

        if (adStructures.size() > 2) {
            // store in the BeaconFrame only the relevant frame structure
            ADStructure frameMainStruct = adStructures.get(2);

            if (!FrameExists()){
                this.addNewFrame(frameMainStruct, data);
                return true;
            } else {
                // modify existing frame
                //String frametype = Integer.toString(this.getFrameType() & 0xFF);
                BeaconFrame frame = getEddystoneFrame(this.getFrameType());
                frame.setData(data);
                frame.setFrameStructure(frameMainStruct);

                // set the TXPOWER
                byte[] frameData = frameMainStruct.getData();
                if (frameData.length > 4) {
                    this.setTxPower((int)frameData[4]);
                }

                int frameIndex = this.getFrameIndex(this.getFrameType());
                System.out.println("INDEX " + frameIndex + " UPDATED FRAME OF TYPE " + this.getFrameType() + " " + new String(data) + "\n\r");


                Frames.set(frameIndex, frame);
                return true;
            }
        }
        return false;
    }



    public ArrayList<BeaconFrame> getFrames() {
        return this.Frames;
    }

    @Override
    public String getUuid() {

        Log.d("mbeacon", "GET UUID FROM Eddystone");
        return "";
    }


    private Boolean isEddystone() {
        return isEddystone(this.lOfADStructures);
    }

    private Boolean isEddystone(ArrayList<ADStructure> adStructures) {

        // On eddystone the AdStructures are 3
        if (adStructures.size() == 3) {
            byte[] ADStruct = adStructures.get(0).getData();
            if (ADStruct.length > 1) {
                // Flags data type value
                if (((int) ADStruct[0] & 0xff) == 0x01) {

                    ADStruct = adStructures.get(1).getData();
                    if (ADStruct.length > 1) {
                        //Complete list of 16-bit Service UUIDs data type value
                        if (((int) ADStruct[0] & 0xff) == 0x03) {

                            ADStruct = adStructures.get(2).getData();
                            if (ADStruct.length > 1) {
                            // Service Data data type value
                                if (((int) ADStruct[0] & 0xff) == 0x16) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public Boolean isBeaconOfType() {

        if (isEddystone()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Boolean isBeaconOfType(ArrayList<ADStructure> adStructures) {

        if (isEddystone(adStructures)) {
            return true;
        } else {
            return false;
        }
    }


    // TLM Frame
    private BeaconFrame getEddystoneFrame(Byte frametype) {

        ArrayList<BeaconFrame> frames = this.getFrames();

        int frameIndex = getFrameIndex(frametype);
        if (frameIndex > -1) {
            System.out.println("GETTING FRAME AT INDEX " + frameIndex  + " DATA " + new String(frames.get(frameIndex).getData()) + "\n\r");

            return frames.get(frameIndex);
        } else {
            return null;
        }
    }

    private byte[] getEddystoneTLMFrameData() {

        BeaconFrame myframe = this.getEddystoneFrame((byte)0x20);
        if (myframe != null) {
            return myframe.getFrameStructure().getData();
        } else {
            return null;
        }
    }

    private byte[] getEddystoneURLFrameData() {

        BeaconFrame myframe = this.getEddystoneFrame((byte)0x10);
        if (myframe != null) {
            return myframe.getFrameStructure().getData();
        } else {
            return null;
        }
    }

    private byte[] getEddystoneUIDFrameData() {

        System.out.println("FROM UID GET "  + "\n\r");
        BeaconFrame myframe = this.getEddystoneFrame((byte)0x00);
        if (myframe != null) {
            return myframe.getFrameStructure().getData();
        } else {
            return null;
        }
    }

    private byte[] getEddystoneEIDFrameData() {

        BeaconFrame myframe = this.getEddystoneFrame((byte)0x30);
        if (myframe != null) {
            return myframe.getFrameStructure().getData();
        } else {
            return null;
        }
    }

    private int getFrameIndex(Byte frameType) {

        Byte type = frameType;
        ArrayList<Byte> frameTypes = this.FrameTypes;
        int i = 0;

        for(Byte b : frameTypes){
            if (b.equals(type)) {
                return i;
            }
            i++;
            //something here
        }
        return -1;

    }


    @Override
    public String getEncodedUrl() {

        byte[] frame = this.getEddystoneURLFrameData();

        if (frame != null) {
            byte[] framedata = Arrays.copyOfRange(frame, 6, 23);
             try {
                 return new String(framedata, "UTF-8");
             } catch (UnsupportedEncodingException e) {
                 System.out.println(e);
                 return null;
             }
        } else {
            return null;
        }

    }


    @Override
    public int getUrlPrefix() {

        byte[] frame = this.getEddystoneURLFrameData();

        if (frame != null) {
            int value = frame[5] & 0xff;
            this.setUrlPrefix(value);
            return value;
        } else {
            return -1;

        }
    }

    @Override
    public String getNamespace() {

        byte[] frame = this.getEddystoneUIDFrameData();

        if (frame != null) {
            byte[] framedata = Arrays.copyOfRange(frame, 5, 15);
            String value = bytesToHex(framedata);
            this.setNamespace(value);
            return value;

        } else {
            return null;
        }

    }

    @Override
    public String getInstance() {

        byte[] frame = this.getEddystoneUIDFrameData();
        if (frame != null) {
            byte[] framedata = Arrays.copyOfRange(frame, 15, 21);
            String value = bytesToHex(framedata);
            this.setInstance(value);

            return value;
        } else {
            return null;
        }
    }

    @Override
    public Short getRFU() {

        byte[] frame = this.getEddystoneUIDFrameData();

        if (frame != null) {
            if (frame.length > 22) {
                byte[] framedata = new byte[2];
                framedata[0]  = frame[21];
                framedata[1]  = frame[22];

                short value = java.nio.ByteBuffer.wrap(framedata).order(ByteOrder.BIG_ENDIAN).getShort();
                this.setRFU(value);
                return value;
            }
            else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Integer getPDUcount() {

        // Beacon temperature
        byte[] frame = this.getEddystoneTLMFrameData();

        if (frame != null) {
            byte[] framedata = new byte[4];
            framedata[0] =  frame[9];
            framedata[1] = frame[10];
            framedata[2] = frame[11];
            framedata[3] = frame[12];

            //Integer value = ((b1 << 24) | (b2 << 16) | (b3 << 8) | (b4)) & 0xffffffffL;
            Integer value = java.nio.ByteBuffer.wrap(framedata).order(ByteOrder.BIG_ENDIAN).getInt();
            this.setPDUcount(value);

            return value;
        } else {
            return null;
        }
    }


    @Override
    public Integer getVersion() {

        byte[] frame = this.getEddystoneTLMFrameData();

        if (frame != null) {
            int value = frame[4] & 0xff;
            this.setVersion(value);
            return value;
        } else {
            return -1;
        }
    }


    @Override
    public Short getBatteryValue() {

        // Battery voltage, 1 mV/bit
        byte[] frame = this.getEddystoneTLMFrameData();

        if (frame != null) {
            byte[] framedata = new byte[2];
            framedata[0]  = frame[5];
            framedata[1]  = frame[6];

            short value = java.nio.ByteBuffer.wrap(framedata).order(ByteOrder.BIG_ENDIAN).getShort();
            this.setBatteryValue(value);

            return value;
        } else {
            return null;
        }
    }

    @Override
    public Short getTemperatureValue() {

        // Beacon temperature
        byte[] frame = this.getEddystoneTLMFrameData();

        if (frame != null) {
            byte[] framedata = new byte[2];
            framedata[0] = frame[7];
            framedata[1] = frame[8];
            short value = java.nio.ByteBuffer.wrap(framedata).order(ByteOrder.LITTLE_ENDIAN).getShort();

            this.setTemperatureValue(value);
            return value;

        } else {
            return null;
        }
    }

    @Override
    public Integer getTime() {

        // Time
        byte[] frame = this.getEddystoneTLMFrameData();
        if (frame != null) {

            byte[] framedata = new byte[4];
            framedata[0] =  frame[13];
            framedata[1] = frame[14];
            framedata[2] = frame[15];
            framedata[3] = frame[16];

            //Integer value = ((b1 << 24) | (b2 << 16) | (b3 << 8) | (b4)) & 0xffffffffL;
            Integer value = java.nio.ByteBuffer.wrap(framedata).order(ByteOrder.BIG_ENDIAN).getInt();

            //Long value = ((b1 << 24) | (b2 << 16) | (b3 << 8) | (b4)) & 0xffffffffL;
            //value = value * 100;
            this.setTime(value);

            return value;
        } else {
            return null;
        }
    }

    @Override
    public String getEphemeralID() {

        byte[] frame = this.getEddystoneEIDFrameData();
        if (frame != null) {
            byte[] framedata = Arrays.copyOfRange(frame, 5, 13);
            String value = bytesToHex(framedata);
            this.setEphemeralID(value);

            return value;
        } else {
            return null;
        }

    }


    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
